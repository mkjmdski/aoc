with open("input.txt", "r") as _f:
    depths = [int(depth) for depth in _f.readlines()]

increased = 0
last_depth = depths[0]
for depth in depths[1:]:
    if depth > last_depth:
        increased += 1
    last_depth = depth

print(increased)
