from collections import defaultdict
with open("input.txt", "r") as _f:
    depths = [int(depth) for depth in _f.readlines()]

sums = defaultdict(int)

index_counter = {
    1: {
        'pointer': 0,
        'moved': 0,
        'enabled': True,
    },
    2: {
        'pointer': 1,
        'moved': 0,
        'enabled': False,
    },
    3: {
        'pointer': 2,
        'moved': 0,
        'enabled': False,
    }
}

for index in range(len(depths)):
    if index > 0:
        index_counter[2]['enabled'] = True
    if index > 1:
        index_counter[3]['enabled'] = True
    for index_address, index_settings in index_counter.items():
        if index_settings['enabled']:
            sums[index_settings['pointer']] += depths[index]
            index_settings['moved'] += 1
        if index_settings['moved'] == 3:
            index_settings['moved'] = 0
            index_settings['pointer'] += 3


depths_sums = list(sums.values())
increased = 0
last_depth = depths_sums[0]
for depth in depths_sums[1:]:
    if depth > last_depth:
        increased += 1
    last_depth = depth

print(increased)
