from collections import defaultdict
moving = defaultdict(int)
with open('input.txt', 'r') as _f:
    for line in _f.readlines():
        command, value = line.split()
        if command not in moving:
            moving[command] = 0
        moving[command] += int(value)

print(moving['forward'] * (moving['down'] - moving['up']))
