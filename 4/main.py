moving = {
    'aim' : 0,
    'forward': 0,
    'depth': 0
}
with open('input.txt', 'r') as _f:
    for line in _f.readlines():
        command, value = line.split()
        value = int(value)
        if command == 'up':
            moving['aim'] -= value
        elif command == 'down':
            moving['aim'] += value
        else:
            moving['forward'] += value
            moving['depth'] += moving['aim'] * value

print(moving['forward'] * moving['depth'])
