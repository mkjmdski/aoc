#!/usr/bin/env python3

gamma = ''
epsilon = ''

matrix = []

with open('input.txt', 'r') as _f:
    for line in _f.readlines():
        matrix.append([int(elem) for elem in list(line.strip())])


for vertical in range(len(matrix[0])):
    sums = {
        0: 0,
        1: 0,
    }
    for horizontal in matrix:
        sums[horizontal[vertical]] += 1
    if sums[0] > sums[1]:
        gamma += '0'
        epsilon += '1'
    else:
        gamma += '1'
        epsilon += '0'

print(int(gamma, 2) * int(epsilon, 2))
