#!/usr/bin/env python3
from copy import deepcopy
import copy

matrix = []
with open("input.txt", "r") as _f:
    for line in _f.readlines():
        matrix.append(list(line.strip()))


def get_ratings(copy_matrix):
    gamma = ""
    epsilon = ""
    for vertical in range(len(copy_matrix[0])):
        sums = {
            "0": 0,
            "1": 0,
        }
        for horizontal in copy_matrix:
            sums[horizontal[vertical]] += 1
        if sums["0"] > sums["1"]:
            gamma += "0"
            epsilon += "1"
        elif sums["0"] < sums["1"]:
            gamma += "1"
            epsilon += "0"
        else:
            gamma += "2"
            epsilon += "2"
    return gamma, epsilon


def find_number(copy_matrix, is_gamma=True, favoured=1):
    copy_matrix_final = deepcopy(copy_matrix)
    for vertical in range(len(copy_matrix[0])):
        gamma, epsilon = get_ratings(copy_matrix)
        popular = gamma[vertical] if is_gamma else epsilon[vertical]
        if popular == "2":
            popular = favoured
        for number in copy_matrix:
            if number[vertical] != popular:
                copy_matrix_final.remove(number)
                if len(copy_matrix_final) == 1:
                    return int("".join(copy_matrix_final[0]), 2)
        copy_matrix = deepcopy(copy_matrix_final)


print(find_number(matrix) * find_number(matrix, False, 0))
